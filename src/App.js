import React from 'react';
import * as Sentry from '@sentry/browser';
import logo from './logo.svg';
import './App.css';

Sentry.init({dsn: "https://6128ee87de5246e8a8c04bad50ed320a@sentry.io/1794956"});

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Edit <code>src/App.js</code> and save to reload.
        </p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
      </header>
    </div>
  );
}

export default App;
